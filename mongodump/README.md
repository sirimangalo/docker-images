# Mongodump

Image to connect to a MongoDB instance, create a database dump and upload it to DigitalOcean Spaces.

## Environment Variables

All variables except `SPACES_PATH_PREFIX`, `SUCCESS_WEBHOOK` are **required**.

| name | default | description |
| --- | :-: | --- |
| `MONGO_HOST` | "mongo" | Hostname of instance to connect to. |
| `MONGO_PORT` | 27017 | Port of instance to connect to. |
| `MONGO_USER` | "mongo" | Username used to authenticate to instance. By default `?authSource=admin` is used. |
| `MONGO_PASSWORD` | "" | Password used to authenticate to instance. |
| `MONGO_DB` | "" | Name of database to back up. |
| `SPACES_KEY` | "" | An [access key](https://www.digitalocean.com/community/tutorials/how-to-create-a-digitalocean-space-and-api-key) for Spaces. |
| `SPACES_SECRET` | "" | The secret for your access key. |
| `SPACES_BUCKET_NAME` | "" | The name of your bucket / space. |
| `SPACES_REGION` | "nyc3" | The region of your bucket / space. |
| `SPACES_PATH_PREFIX` | "" | A prefix / path under which dumps should be created. Should end with "/". |
| `SUCCESS_WEBHOOK` | "" | URL to call after successful execution. |

## Setting Lifecycle Policy

In order to automatically remove old dumps, do this:

1. Create a file `lifecycle.xml`

```
<LifecycleConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
  <Rule>
    <ID>Expire old dumps</ID>
    <Status>Enabled</Status>
    <Prefix></Prefix>
    <Expiration>
      <Days>90</Days>
    </Expiration>
  </Rule>
</LifecycleConfiguration>
```

- Set number of days as needed
- **Make sure** to insert your `SPACES_PATH_PREFIX` into the `<Prefix>` tag

2. Show current policy

```
docker run -e SPACES_KEY="..." -e SPACES_SECRET="..." -e SPACES_REGION="nyc3" -e SPACES_BUCKET_NAME="mybucket" -v $(pwd):/opt/backups --rm mongodump ./s3cmd.sh getlifecycle s3://mybucket
```

3. Update policy

```
docker run -e SPACES_KEY="..." -e SPACES_SECRET="..." -e SPACES_REGION="nyc3" -e SPACES_BUCKET_NAME="mybucket" -v $(pwd):/opt/backups --rm mongodump ./s3cmd.sh setlifecycle lifecycle.xml s3://mybucket
```

4. Repeat step 2 to check policy