#!/bin/bash

set -e

sed -i "s/SPACES_REGION/${SPACES_REGION}/g" .s3cfg
sed -i "s#SPACES_KEY#${SPACES_KEY}#g" .s3cfg
sed -i "s#SPACES_SECRET#${SPACES_SECRET}#g" .s3cfg

s3cmd -c .s3cfg "$@"