#!/bin/bash

set -e

sed -i "s/SPACES_REGION/${SPACES_REGION}/g" .s3cfg
sed -i "s@SPACES_KEY@${SPACES_KEY}@g" .s3cfg
sed -i "s@SPACES_SECRET@${SPACES_SECRET}@g" .s3cfg

CURRENT_DATE=$(date +%Y%m%d_%H%M%S)
FILE_NAME="${MONGO_DB}_${CURRENT_DATE}.gz"

# TODO: add --oplog (Failed: error getting oplog start: not found)

mongodump \
  --authenticationDatabase='admin' \
  --host="${MONGO_HOST}" \
  --username="${MONGO_USER}" \
  --port="${MONGO_PORT}" \
  --password="${MONGO_PASSWORD}" \
  --db="${MONGO_DB}" \
  --gzip \
  --archive="${FILE_NAME}"

s3cmd -c .s3cfg put "${FILE_NAME}" "s3://${SPACES_BUCKET_NAME}/${SPACES_PATH_PREFIX}"
rm "${FILE_NAME}"

if [[ ! -z "${SUCCESS_WEBHOOK}" ]]; then
  curl -fsS -m 10 --retry 5 -o /dev/null "${SUCCESS_WEBHOOK}"
fi
