# Mongodump

Image to connect to a MYSQL instance, create a database dump and upload it to a S3 storage.

## Environment Variables

All variables except `S3_PATH_PREFIX`, `SUCCESS_WEBHOOK` are **required**.

| name | description |
| --- | --- |
| `MYSQL_HOST` | Hostname of instance to connect to. |
| `MYSQL_PORT` | Port of instance to connect to. |
| `MYSQL_USER` |  Username used to authenticate to instance. By default `?authSource=admin` is used. |
| `MYSQL_PASSWORD` | Password used to authenticate to instance. |
| `MYSQL_DB` | Name of database to back up. |
| `S3_ACCESS_KEY` | An S3 access key. |
| `S3_SECRET_KEY` |  The secret for your access key. |
| `S3_BUCKET_NAME` | The name of your bucket. |
| `S3_REGION` |  The region of your bucket. |
| `S3_PATH_PREFIX` | A prefix / path under which dumps should be created. Should end with "/". |
| `SUCCESS_WEBHOOK` | URL to call after successful execution. |

## Setting Lifecycle Policy

In order to automatically remove old dumps, do this:

1. Create a file `lifecycle.xml`

```
<LifecycleConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
  <Rule>
    <ID>Expire old dumps</ID>
    <Status>Enabled</Status>
    <Prefix></Prefix>
    <Expiration>
      <Days>90</Days>
    </Expiration>
  </Rule>
</LifecycleConfiguration>
```

- Set number of days as needed
- **Make sure** to insert your `S3_PATH_PREFIX` into the `<Prefix>` tag

2. Show current policy

```
docker run -e S3_ACCESS_KEY="..." -e S3_SECRET_KEY="..." -e S3_REGION="nyc3" -e S3_BUCKET_NAME="mybucket" -v $(pwd):/opt/backups --rm mongodump ./s3cmd.sh getlifecycle s3://mybucket
```

3. Update policy

```
docker run -e S3_ACCESS_KEY="..." -e S3_SECRET_KEY="..." -e S3_REGION="nyc3" -e S3_BUCKET_NAME="mybucket" -v $(pwd):/opt/backups --rm mongodump ./s3cmd.sh setlifecycle lifecycle.xml s3://mybucket
```

4. Repeat step 2 to check policy
