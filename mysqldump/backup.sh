#!/bin/bash

set -e
set -o pipefail

CURRENT_DATE=$(date +%Y%m%d_%H%M%S)
FILE_NAME="${MYSQL_DB}_${CURRENT_DATE}"

# IMPORTANT: Do not add a space after "-p". It might treat that as part of the password. Yes, really...
mysqldump \
  -h "${MYSQL_HOST}" \
  -P "${MYSQL_PORT}" \
  -u "${MYSQL_USER}" \
  -p"${MYSQL_PASSWORD}" \
  --databases "${MYSQL_DB}" | gzip -c > "${FILE_NAME}.sql.gz"

if [ ! -s "${FILE_NAME}.sql.gz" ]; then
  echo "Error: exported file is empty"
  exit 1
fi

s3cmd \
  --access_key="${S3_ACCESS_KEY}" \
  --secret_key="${S3_SECRET_KEY}" \
  --region="${S3_REGION}" \
  --host="${S3_HOST}" \
  --host-bucket="%(bucket)s.${S3_REGION}.${S3_HOST}" \
  put "${FILE_NAME}.sql.gz" "s3://${S3_BUCKET_NAME}/${S3_PATH_PREFIX%/}/${FILE_NAME}.sql.gz"

if [ ! -z "${SUCCESS_WEBHOOK}" ]; then
  curl -fsS -m 10 --retry 5 -o /dev/null "${SUCCESS_WEBHOOK}"
fi
